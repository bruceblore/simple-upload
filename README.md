# simple-upload

A script to re-build and upload your package without having to remember the exact commands

## Getting Started

### Prerequisites

No dependencies. Yay!

### Installing

```
pip install simple-upload
```

## Running

```
pip-upload
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
